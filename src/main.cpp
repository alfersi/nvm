#include <Arduino.h>
#include <EEPROM.h>
#include "NVM.h"

NVM ReespiratorNVM;

void setup() 
{
  Serial.begin(115200);
  ReespiratorNVM.Begin();
}

void loop() 
{
  delay(4);
  ReespiratorNVM.Update();
}