#include <Arduino.h>
#include <EEPROM.h>
#include "NVM.h"
#include "src_reespirator_SerialMonitor_crc16.h"

sNVM Parameters;

NVM::NVM(void)
{
    EEPROM.begin();
}

NVM::~NVM(void)
{
    EEPROM.end();
}

void NVM::Begin(void)
{
    unsigned int CRC0,CRC1;
    unsigned int DefaultIndex;
    unsigned char *pNVMContents,*pParameters,*pDefault;

    EEPROM.get(0,_NVMContents);
    CRC0=crc16(0xFFFF,(const unsigned char*)&(_NVMContents.Parameters),sizeof(_NVMContents.Parameters));
    if(CRC0==_NVMContents.CRC)
    {
        Parameters=_NVMContents.Parameters;
        EEPROM.get(NVM_BACKUP_OFFSET,_NVMContents);
        CRC1=crc16(0xFFFF,(const unsigned char*)&(_NVMContents.Parameters),sizeof(_NVMContents.Parameters));
        if(CRC1!=_NVMContents.CRC) 
        {
            _NVMContents.Parameters=Parameters;
            _NVMContents.CRC=CRC0;
            EEPROM.put(NVM_BACKUP_OFFSET,_NVMContents);
        }
    }
    else
    {
        EEPROM.get(NVM_BACKUP_OFFSET,_NVMContents);
        CRC1=crc16(0xFFFF,(const unsigned char*)&(_NVMContents.Parameters),sizeof(_NVMContents.Parameters));
        if(CRC1==_NVMContents.CRC)
        {
            Parameters=_NVMContents.Parameters;
            EEPROM.put(0,_NVMContents);
        }
        else
        {
            pNVMContents=(unsigned char*)&(_NVMContents.Parameters);
            pParameters=(unsigned char*)&Parameters;
            pDefault=(unsigned char*)&DefaultParameters;
            for(DefaultIndex=0;DefaultIndex<sizeof(DefaultParameters);DefaultIndex++)
            {
                *pNVMContents++=*pParameters++=pgm_read_byte_far(pDefault++);
            }
            _NVMContents.CRC=crc16(0xFFFF,(const unsigned char*)&(_NVMContents.Parameters),sizeof(_NVMContents.Parameters));
            EEPROM.put(0,_NVMContents);
            EEPROM.put(NVM_BACKUP_OFFSET,_NVMContents);
        }        
    }
}

void NVM::Update(void)
{
    static unsigned int Address=0;
    static unsigned char *pNVMContents=(unsigned char*)&(_NVMContents.Parameters);
    unsigned char *pParameters=(unsigned char*)&Parameters;

    if(Address==0)
    {
        pNVMContents=(unsigned char*)&(_NVMContents.Parameters);
        pParameters=(unsigned char*)&Parameters;
        while(*pNVMContents++==*pParameters++)
        {
            if(++Address>=sizeof(Parameters)) break;
        }
        if(Address<sizeof(Parameters))
        {
            *pNVMContents--;
            *pParameters--;
            while(++Address<=sizeof(Parameters))
            {
                *pNVMContents++=*pParameters++;
            }      
            _NVMContents.CRC=crc16(0xFFFF,(const unsigned char*)&(_NVMContents.Parameters),sizeof(_NVMContents.Parameters));
            pNVMContents=(unsigned char*)&_NVMContents;
            Address=0;
            EEPROM.update(Address++,*pNVMContents++);
        }
        else Address=0;
    }
    else if(Address<sizeof(_NVMContents))
    {
        EEPROM.update(Address++,*pNVMContents++);
    }
    else if(Address<NVM_BACKUP_OFFSET)
    {
        Address=NVM_BACKUP_OFFSET;
        pNVMContents=(unsigned char*)&_NVMContents;
        EEPROM.update(Address++,*pNVMContents++);
    }
    else if(Address<(NVM_BACKUP_OFFSET+sizeof(_NVMContents)))
    {
        EEPROM.update(Address++,*pNVMContents++);
    }
    else Address=0;
}