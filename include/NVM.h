#ifndef NVM_H
#define NVM_H

#define NVM_BACKUP_OFFSET ((E2END+1)/2)

typedef struct
{
    unsigned int Peak;
    unsigned int Peep;
    unsigned int RPM;
    unsigned int Volume;
    unsigned int Trigger;
    unsigned int Ramp;
    unsigned int IE;
    unsigned int VentilationMode;
    unsigned int Stop;
    unsigned int Recruit;
    unsigned int Mute;
    unsigned int UnderVolumeAlarmThreshold;
    unsigned int OverVolumeAlarmThreshold;
    unsigned int UnderRPMAlarmThreshold;
    unsigned int OverRPMAlarmThreshold;
    unsigned int OverPressureAlarmThreshold;
    unsigned int UnderPressureAlarmThreshold;
}sNVM;

const PROGMEM sNVM DefaultParameters=
{   
    .Peak=0x0000,
    .Peep=0x0000,
    .RPM=0x0000,
    .Volume=0x0000,
    .Trigger=0x0000,
    .Ramp=0x0000,
    .IE=0x0000,
    .VentilationMode=0x0000,
    .Stop=0x0000,
    .Recruit=0x0000,
    .Mute=0x0000,
    .UnderVolumeAlarmThreshold=0x0000,
    .OverVolumeAlarmThreshold=0x0000,
    .UnderRPMAlarmThreshold=0x0000,
    .OverRPMAlarmThreshold=0x0000,
    .OverPressureAlarmThreshold=0x0000,
    .UnderPressureAlarmThreshold=0x0000
};

const PROGMEM sNVM MinimumParameters=
{   
    .Peak=0x0000,
    .Peep=0x0000,
    .RPM=0x0000,
    .Volume=0x0000,
    .Trigger=0x0000,
    .Ramp=0x0000,
    .IE=0x0000,
    .VentilationMode=0x0000,
    .Stop=0x0000,
    .Recruit=0x0000,
    .Mute=0x0000,
    .UnderVolumeAlarmThreshold=0x0000,
    .OverVolumeAlarmThreshold=0x0000,
    .UnderRPMAlarmThreshold=0x0000,
    .OverRPMAlarmThreshold=0x0000,
    .OverPressureAlarmThreshold=0x0000,
    .UnderPressureAlarmThreshold=0x0000
};

const PROGMEM sNVM MaximumParameters=
{   
    .Peak=0xFFFF,
    .Peep=0xFFFF,
    .RPM=0xFFFF,
    .Volume=0xFFFF,
    .Trigger=0xFFFF,
    .Ramp=0xFFFF,
    .IE=0xFFFF,
    .VentilationMode=0xFFFF,
    .Stop=0xFFFF,
    .Recruit=0xFFFF,
    .Mute=0xFFFF,
    .UnderVolumeAlarmThreshold=0xFFFF,
    .OverVolumeAlarmThreshold=0xFFFF,
    .UnderRPMAlarmThreshold=0xFFFF,
    .OverRPMAlarmThreshold=0xFFFF,
    .OverPressureAlarmThreshold=0xFFFF,
    .UnderPressureAlarmThreshold=0xFFFF
};

extern sNVM Parameters;

class NVM
{
    public:
    NVM(void);
    ~NVM(void);
    void Begin(void);
    void Update(void);
    private:
    struct
    {
        sNVM Parameters;
        unsigned int CRC;
    } _NVMContents;
};

#endif
